/********************************************************************************
** Form generated from reading UI file 'neoragnaryk.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEORAGNARYK_H
#define UI_NEORAGNARYK_H

#include <QtCore/QVariant>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NeoRagnaryk
{
public:
    QAction *actionAbout_NeoRagnaryk;
    QAction *actionrefresh;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QProgressBar *progressBar;
    QWebEngineView *browser;
    QHBoxLayout *horizontalLayout;
    QPushButton *back_btn;
    QPushButton *forward_btn;
    QLineEdit *url_edit;
    QPushButton *go_btn;
    QPushButton *refresh_btn;
    QPushButton *settings_btn;
    QMenuBar *menuBar;
    QMenu *menuAbout;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *NeoRagnaryk)
    {
        if (NeoRagnaryk->objectName().isEmpty())
            NeoRagnaryk->setObjectName(QString::fromUtf8("NeoRagnaryk"));
        NeoRagnaryk->resize(1280, 800);
        actionAbout_NeoRagnaryk = new QAction(NeoRagnaryk);
        actionAbout_NeoRagnaryk->setObjectName(QString::fromUtf8("actionAbout_NeoRagnaryk"));
        actionrefresh = new QAction(NeoRagnaryk);
        actionrefresh->setObjectName(QString::fromUtf8("actionrefresh"));
        centralWidget = new QWidget(NeoRagnaryk);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setEnabled(true);
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy);
        progressBar->setMaximumSize(QSize(16777215, 8));
        progressBar->setStyleSheet(QString::fromUtf8("border-radius: 0px;"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        progressBar->setInvertedAppearance(false);

        gridLayout->addWidget(progressBar, 1, 0, 1, 1);

        browser = new QWebEngineView(centralWidget);
        browser->setObjectName(QString::fromUtf8("browser"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(browser->sizePolicy().hasHeightForWidth());
        browser->setSizePolicy(sizePolicy1);
        browser->setUrl(QUrl(QString::fromUtf8("about:blank")));

        gridLayout->addWidget(browser, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        back_btn = new QPushButton(centralWidget);
        back_btn->setObjectName(QString::fromUtf8("back_btn"));
        back_btn->setFlat(true);

        horizontalLayout->addWidget(back_btn);

        forward_btn = new QPushButton(centralWidget);
        forward_btn->setObjectName(QString::fromUtf8("forward_btn"));
        forward_btn->setFlat(true);

        horizontalLayout->addWidget(forward_btn);

        url_edit = new QLineEdit(centralWidget);
        url_edit->setObjectName(QString::fromUtf8("url_edit"));
        url_edit->setStyleSheet(QString::fromUtf8("border: 1px solid #888;"));
        url_edit->setFrame(false);
        url_edit->setClearButtonEnabled(true);

        horizontalLayout->addWidget(url_edit);

        go_btn = new QPushButton(centralWidget);
        go_btn->setObjectName(QString::fromUtf8("go_btn"));
        go_btn->setFlat(true);

        horizontalLayout->addWidget(go_btn);

        refresh_btn = new QPushButton(centralWidget);
        refresh_btn->setObjectName(QString::fromUtf8("refresh_btn"));
        refresh_btn->setFlat(true);

        horizontalLayout->addWidget(refresh_btn);

        settings_btn = new QPushButton(centralWidget);
        settings_btn->setObjectName(QString::fromUtf8("settings_btn"));
        settings_btn->setFlat(true);

        horizontalLayout->addWidget(settings_btn);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        NeoRagnaryk->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(NeoRagnaryk);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1280, 28));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        NeoRagnaryk->setMenuBar(menuBar);
        statusBar = new QStatusBar(NeoRagnaryk);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        NeoRagnaryk->setStatusBar(statusBar);

        menuBar->addAction(menuAbout->menuAction());
        menuAbout->addAction(actionAbout_NeoRagnaryk);

        retranslateUi(NeoRagnaryk);

        QMetaObject::connectSlotsByName(NeoRagnaryk);
    } // setupUi

    void retranslateUi(QMainWindow *NeoRagnaryk)
    {
        NeoRagnaryk->setWindowTitle(QApplication::translate("NeoRagnaryk", "NeoRagnaryk", nullptr));
        actionAbout_NeoRagnaryk->setText(QApplication::translate("NeoRagnaryk", "About NeoRagnaryk", nullptr));
        actionrefresh->setText(QApplication::translate("NeoRagnaryk", "refresh", nullptr));
        back_btn->setText(QString());
        forward_btn->setText(QString());
        go_btn->setText(QString());
        refresh_btn->setText(QString());
        settings_btn->setText(QString());
        menuAbout->setTitle(QApplication::translate("NeoRagnaryk", "About", nullptr));
    } // retranslateUi

};

namespace Ui {
    class NeoRagnaryk: public Ui_NeoRagnaryk {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEORAGNARYK_H
