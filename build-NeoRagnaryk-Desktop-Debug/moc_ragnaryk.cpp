/****************************************************************************
** Meta object code from reading C++ file 'ragnaryk.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/ragnaryk.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ragnaryk.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Ragnaryk_t {
    QByteArrayData data[20];
    char stringdata0[375];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Ragnaryk_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Ragnaryk_t qt_meta_stringdata_Ragnaryk = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Ragnaryk"
QT_MOC_LITERAL(1, 9, 17), // "on_go_btn_clicked"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 19), // "on_back_btn_clicked"
QT_MOC_LITERAL(4, 48, 22), // "on_forward_btn_clicked"
QT_MOC_LITERAL(5, 71, 22), // "on_refresh_btn_clicked"
QT_MOC_LITERAL(6, 94, 23), // "on_browser_loadProgress"
QT_MOC_LITERAL(7, 118, 8), // "progress"
QT_MOC_LITERAL(8, 127, 33), // "on_actionAbout_Ragnaryk_trigg..."
QT_MOC_LITERAL(9, 161, 21), // "on_browser_urlChanged"
QT_MOC_LITERAL(10, 183, 4), // "arg1"
QT_MOC_LITERAL(11, 188, 25), // "on_url_edit_returnPressed"
QT_MOC_LITERAL(12, 214, 23), // "on_browser_loadFinished"
QT_MOC_LITERAL(13, 238, 22), // "on_browser_loadStarted"
QT_MOC_LITERAL(14, 261, 23), // "acceptFullScreenRequest"
QT_MOC_LITERAL(15, 285, 27), // "QWebEngineFullScreenRequest"
QT_MOC_LITERAL(16, 313, 7), // "request"
QT_MOC_LITERAL(17, 321, 19), // "fullScreenRequested"
QT_MOC_LITERAL(18, 341, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(19, 369, 5) // "index"

    },
    "Ragnaryk\0on_go_btn_clicked\0\0"
    "on_back_btn_clicked\0on_forward_btn_clicked\0"
    "on_refresh_btn_clicked\0on_browser_loadProgress\0"
    "progress\0on_actionAbout_Ragnaryk_triggered\0"
    "on_browser_urlChanged\0arg1\0"
    "on_url_edit_returnPressed\0"
    "on_browser_loadFinished\0on_browser_loadStarted\0"
    "acceptFullScreenRequest\0"
    "QWebEngineFullScreenRequest\0request\0"
    "fullScreenRequested\0on_tabWidget_currentChanged\0"
    "index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Ragnaryk[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x08 /* Private */,
       3,    0,   80,    2, 0x08 /* Private */,
       4,    0,   81,    2, 0x08 /* Private */,
       5,    0,   82,    2, 0x08 /* Private */,
       6,    1,   83,    2, 0x08 /* Private */,
       8,    0,   86,    2, 0x08 /* Private */,
       9,    1,   87,    2, 0x08 /* Private */,
      11,    0,   90,    2, 0x08 /* Private */,
      12,    1,   91,    2, 0x08 /* Private */,
      13,    0,   94,    2, 0x08 /* Private */,
      14,    1,   95,    2, 0x08 /* Private */,
      17,    1,   98,    2, 0x08 /* Private */,
      18,    1,  101,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, QMetaType::Int,   19,

       0        // eod
};

void Ragnaryk::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Ragnaryk *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_go_btn_clicked(); break;
        case 1: _t->on_back_btn_clicked(); break;
        case 2: _t->on_forward_btn_clicked(); break;
        case 3: _t->on_refresh_btn_clicked(); break;
        case 4: _t->on_browser_loadProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_actionAbout_Ragnaryk_triggered(); break;
        case 6: _t->on_browser_urlChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 7: _t->on_url_edit_returnPressed(); break;
        case 8: _t->on_browser_loadFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_browser_loadStarted(); break;
        case 10: _t->acceptFullScreenRequest((*reinterpret_cast< QWebEngineFullScreenRequest(*)>(_a[1]))); break;
        case 11: _t->fullScreenRequested((*reinterpret_cast< QWebEngineFullScreenRequest(*)>(_a[1]))); break;
        case 12: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Ragnaryk::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_Ragnaryk.data,
    qt_meta_data_Ragnaryk,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Ragnaryk::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Ragnaryk::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Ragnaryk.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Ragnaryk::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
