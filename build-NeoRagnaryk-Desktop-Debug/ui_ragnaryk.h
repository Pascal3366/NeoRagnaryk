/********************************************************************************
** Form generated from reading UI file 'ragnaryk.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RAGNARYK_H
#define UI_RAGNARYK_H

#include <QtCore/QVariant>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Ragnaryk
{
public:
    QAction *actionAbout_Ragnaryk;
    QAction *actionrefresh;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWebEngineView *browser;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *back_btn;
    QPushButton *forward_btn;
    QPushButton *refresh_btn;
    QLineEdit *url_edit;
    QPushButton *go_btn;
    QPushButton *settings_btn;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuAbout;

    void setupUi(QMainWindow *Ragnaryk)
    {
        if (Ragnaryk->objectName().isEmpty())
            Ragnaryk->setObjectName(QString::fromUtf8("Ragnaryk"));
        Ragnaryk->resize(1280, 800);
        actionAbout_Ragnaryk = new QAction(Ragnaryk);
        actionAbout_Ragnaryk->setObjectName(QString::fromUtf8("actionAbout_Ragnaryk"));
        actionrefresh = new QAction(Ragnaryk);
        actionrefresh->setObjectName(QString::fromUtf8("actionrefresh"));
        centralWidget = new QWidget(Ragnaryk);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(centralWidget);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        browser = new QWebEngineView(tab);
        browser->setObjectName(QString::fromUtf8("browser"));
        browser->setGeometry(QRect(0, 33, 1256, 701));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(browser->sizePolicy().hasHeightForWidth());
        browser->setSizePolicy(sizePolicy1);
        browser->setUrl(QUrl(QString::fromUtf8("about:blank")));
        layoutWidget = new QWidget(tab);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 1262, 27));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        back_btn = new QPushButton(layoutWidget);
        back_btn->setObjectName(QString::fromUtf8("back_btn"));
        back_btn->setFlat(true);

        horizontalLayout->addWidget(back_btn);

        forward_btn = new QPushButton(layoutWidget);
        forward_btn->setObjectName(QString::fromUtf8("forward_btn"));
        forward_btn->setFlat(true);

        horizontalLayout->addWidget(forward_btn);

        refresh_btn = new QPushButton(layoutWidget);
        refresh_btn->setObjectName(QString::fromUtf8("refresh_btn"));
        refresh_btn->setFlat(true);

        horizontalLayout->addWidget(refresh_btn);

        url_edit = new QLineEdit(layoutWidget);
        url_edit->setObjectName(QString::fromUtf8("url_edit"));
        url_edit->setStyleSheet(QString::fromUtf8("border: 1px solid #888;"));
        url_edit->setFrame(false);
        url_edit->setClearButtonEnabled(true);

        horizontalLayout->addWidget(url_edit);

        go_btn = new QPushButton(layoutWidget);
        go_btn->setObjectName(QString::fromUtf8("go_btn"));
        go_btn->setFlat(true);

        horizontalLayout->addWidget(go_btn);

        settings_btn = new QPushButton(layoutWidget);
        settings_btn->setObjectName(QString::fromUtf8("settings_btn"));
        settings_btn->setFlat(true);

        horizontalLayout->addWidget(settings_btn);

        progressBar = new QProgressBar(tab);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(0, 27, 1262, 4));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy2);
        progressBar->setMaximumSize(QSize(16777215, 8));
        progressBar->setStyleSheet(QString::fromUtf8("border-radius: 0px;"));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        progressBar->setInvertedAppearance(false);
        tabWidget->addTab(tab, QString());

        horizontalLayout_2->addWidget(tabWidget);

        Ragnaryk->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Ragnaryk);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1280, 22));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        Ragnaryk->setMenuBar(menuBar);

        menuBar->addAction(menuAbout->menuAction());
        menuAbout->addAction(actionAbout_Ragnaryk);

        retranslateUi(Ragnaryk);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Ragnaryk);
    } // setupUi

    void retranslateUi(QMainWindow *Ragnaryk)
    {
        Ragnaryk->setWindowTitle(QApplication::translate("Ragnaryk", "Ragnaryk", nullptr));
        actionAbout_Ragnaryk->setText(QApplication::translate("Ragnaryk", "About Ragnaryk", nullptr));
        actionrefresh->setText(QApplication::translate("Ragnaryk", "refresh", nullptr));
        back_btn->setText(QString());
        forward_btn->setText(QString());
        refresh_btn->setText(QString());
        go_btn->setText(QString());
        settings_btn->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Ragnaryk", "Tab 1", nullptr));
        menuAbout->setTitle(QApplication::translate("Ragnaryk", "About", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Ragnaryk: public Ui_Ragnaryk {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RAGNARYK_H
